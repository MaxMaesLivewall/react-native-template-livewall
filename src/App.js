/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from './store';
import {registerScreens} from "./screens";
import {Navigation} from "react-native-navigation";
import {helloWorldScreen} from "./constants/screens";

const store = configureStore();

registerScreens(store, Provider);

export default class App {

    constructor() {
        App.startApp();
    }

    static startApp() {
        Navigation.events().registerAppLaunchedListener(() => {
            Navigation.setRoot({
                root: {
                    stack: {
                        children: [{
                            component: {
                                name: helloWorldScreen
                            }
                        }]
                    }
                }
            })
        });
    }
}